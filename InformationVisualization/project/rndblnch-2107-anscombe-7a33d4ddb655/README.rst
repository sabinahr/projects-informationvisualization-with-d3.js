==================
Anscombe's quartet
==================

A dataset scrapped from the famous `Graphs in Statistical Analysis`_ by F. J. Anscombe (1973).


Files
-----

- data/anscombe.txt   reproduction of  the original data set
- data/txt2tsv.py     script to generate table from original data set
- data/anscombe.tsv   data set

- viz/0-tables.html   table "visualisation" generated using d3.js_


.. _Graphs in Statistical Analysis: http://iihm.imag.fr/blanch/teaching/infovis/readings/1973-Anscombe-Graphs_in_Stats.pdf
.. _d3.js: http://d3js.org
